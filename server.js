const express = require('express')
const path = require('path')
const axios = require('axios')

const port = process.env.PORT || 5000

const app = express()

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('./node_modules'));

app.get('/', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'public/index.html'))
})

app.get('/movie/*', (req, res) => {
	res.sendFile(path.resolve(__dirname, 'public/index.html'))
})

app.get('/movies', (req, res) => {
	axios.get(`https://api.themoviedb.org/4/discover/movie?sort_by=popularity.desc&api_key=${process.env.TMDBAPI}`)
	.then(response => {
		res.json(response.data)
	})
	.catch(error => {
		console.log(error)
	})
})

app.get('/moviedetails/:id', (req, res) => {
	axios.get(`https://api.themoviedb.org/3/movie/${req.params.id}?api_key=${process.env.TMDBAPI}`)
	.then(response => {
		res.json(response.data)
	})
	.catch(error => {
		console.log(error)
	})
})

app.listen(port);
console.log(`Server started on port ${port}`)