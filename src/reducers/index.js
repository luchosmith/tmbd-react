import { combineReducers } from 'redux'
import MoviesReducer from './movies_reducer.js'
import MovieReducer from './movie_reducer.js'
import ErrorReducer from './error_reducer.js'

const rootReducer = combineReducers({
	movies: MoviesReducer,
	movie: MovieReducer,
	error: ErrorReducer
})

export default rootReducer