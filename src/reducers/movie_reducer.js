import { FETCH_MOVIE } from '../actions'

export default function(state = {}, action){
	switch(action.type){
		case FETCH_MOVIE:
			const movie =  action.payload.data
			return movie
		default:
			return state
	}	
}