import axios from 'axios'

export const FETCH_MOVIES = 'fetch_movies'
export const FETCH_MOVIE = 'fetch_movie'
export const FETCH_MOVIE_ERROR = 'fetch_movie_error'

export function fetchMovies(){

	const request = axios.get(`/movies`).catch(function(error){
		console.log(error)
	})

	return{
		type: FETCH_MOVIES,
		payload: request
	}
}

export function fetchMovie(id){
	let error = {display: false, errorMsg: ''}

	const request = axios.get(`/moviedetails/${id}`)

	if(error.display){
		return {
			type: FETCH_MOVIE_ERROR,
			payload: error
		}
	}else{
		return {
			type: FETCH_MOVIE,
			payload: request
		}
	}
}
