import React from 'react'
import { Link } from 'react-router-dom'

const Movie = (props) => {

	let imgPath = `https://image.tmdb.org/t/p/w300${props.movie.poster_path}`

	return(
		<div className='movie'>
			<Link to={`/movie/${props.movie.id}`}><img className='movieImg' src={imgPath} /></Link>
		</div>
	)
}

export default Movie