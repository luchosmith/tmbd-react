import React from 'react'
import {Route, Switch} from 'react-router-dom'
import Home from './home.js'
import Movie from '../containers/movie.js'


const Main = () => {
	return(
        <div>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/movie/:id' component={Movie} />
            </Switch>
            <footer>
                <div className="copyright">
                    <img src="//www.themoviedb.org/static_cache/v4/logos/primary-green-d70eebe18a5eb5b166d5c1ef0796715b8d1a2cbc698f96d311d62f894ae87085.svg" role="presentation" />
                    <p>This product uses the TMDb API but is not endorsed or certified by TMDb.</p>
                </div>
            </footer>
        </div>
	)
}

export default Main