import React from 'react'
import ReactDOM from 'react-dom'

import Movies from '../containers/movies.js'

const Home = (props) =>{
	return(
		<div>
			<header>
                <h1>Popular Movies</h1>
            </header>
			<div className='container movieContainer'>
				<Movies/>
			</div>
		</div>
	)
}

export default Home