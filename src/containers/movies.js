import React, { Component } from 'react'
import {connect} from 'react-redux'
import { fetchMovies } from '../actions'

import Movie from '../components/movie.js'

class Movies extends Component{

	constructor(props){
		super(props)
	}

	componentWillMount(){
		this.props.fetchMovies()
	}

	render(){
        const movies = this.props.movies
		return(
			<div className='moviesInner'>
				{
                    movies.map( movie => {
                        return (<Movie key={movie.id} movie={movie} />)
                    })
				}
			</div>
		)
	}
}


function mapStateToProps(state){
	return{
		movies: state.movies
	}
}


export default connect(mapStateToProps, { fetchMovies })(Movies)