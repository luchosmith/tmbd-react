import React, {Component} from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { fetchMovie, fetchMovies } from  '../actions'

class Movie extends Component {
	
	componentWillMount(){
		const {id} = this.props.match.params
		this.props.fetchMovie(id)
	}
	
	render(){
        const movie = this.props.movie
        let posterPath = `https://image.tmdb.org/t/p/w300${movie.poster_path}`
        let backdropPath = `https://image.tmdb.org/t/p/w500${movie.backdrop_path}`
		return(
			<div className="movie-container">
				<div className='movie-hero'>
					<div className='movieBackdrop'>
                        <img src={backdropPath} />
					</div>
                    <div className="movie-title">{movie.title}</div>
				</div>
                <div className="movie-poster">
                    <img src={posterPath} />
                </div>
                <div className="movie-details">
                <div className='back-btn-div'><Link to='/'><i className="fa fa-angle-left"></i> movies</Link></div>
                    <h2 className="movie-title">{movie.title}</h2>
                    <p>{movie.overview}</p>
                </div>
			</div>
		)
	}
}

function mapStateToProps(state){
	return {
		movie: state.movie
	}
}


export default connect(mapStateToProps, {fetchMovie})(Movie)