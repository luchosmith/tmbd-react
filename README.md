# The Movie Database / React

### React app build on The Movie Database API

##### Built with:

* React
* Redux
* Node
* Express
* CSS


### TODO:
* include more data eg: cast, genre, rating
* server side rendering
* unit tests
* add dev server